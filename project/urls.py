"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from redirs import views as redir_views
from blackout import views as blackout_views
from website import views as website_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', website_views.index),
    url(r'^blackout/$', website_views.blackout),
    url(r'^blackout/demo$', website_views.blackout_demo),
    url(r'^styl.css$', website_views.styl),
    url(r'^logo.png$', website_views.logo),
    url(r'^blackout.js$', blackout_views.blackout_js),
    url(r'^([^/]+)/$', redir_views.redirect_view),
]
