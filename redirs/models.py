from django.db import models

class Redirect(models.Model):
    key = models.CharField(max_length=200)
    target = models.URLField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.key
