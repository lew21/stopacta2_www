from django.shortcuts import redirect
from django.http import Http404
from .models import *

def home_view(request):
	return redirect("https://www.facebook.com/stopacta2/")

def redirect_view(request, key):
	try:
		redir = Redirect.objects.get(key=key)
		return redirect(redir.target)
	except Redirect.DoesNotExist:
		raise Http404
