from django.contrib import admin
from .models import *

class RedirectAdmin(admin.ModelAdmin):
	list_display = ('key', 'target', 'created_at', 'updated_at')

admin.site.register(Redirect, RedirectAdmin)
