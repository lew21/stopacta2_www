import pkg_resources
from django.http import HttpResponse

def index(request):
    return HttpResponse(pkg_resources.resource_string('website', 'index.html').decode('utf-8'), 'text/html', charset='utf-8')

def blackout(request):
    return HttpResponse(pkg_resources.resource_string('website', 'blackout.html').decode('utf-8'), 'text/html', charset='utf-8')

def blackout_demo(request):
    return HttpResponse(pkg_resources.resource_string('website', 'blackout_demo.html').decode('utf-8'), 'text/html', charset='utf-8')

def styl(request):
    return HttpResponse(pkg_resources.resource_string('website', 'styl.css').decode('utf-8'), 'text/css', charset='utf-8')

def logo(request):
    return HttpResponse(pkg_resources.resource_string('website', 'logo.png'), 'image/png')
