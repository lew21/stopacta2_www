import pkg_resources
from django.db import IntegrityError
from django.http import HttpResponse

from .models import Website

def blackout_js(request):
    try:
        website = Website()
        website.origin = request.META['HTTP_ORIGIN']
        website.save()
    except (IntegrityError, KeyError):
        pass
    except Exception as e:
        print(e)

    res = HttpResponse(pkg_resources.resource_string('blackout', 'blackout.js').decode('utf-8'), 'application/javascript', charset='utf-8')
    res['Access-Control-Allow-Origin'] = '*'
    return res
