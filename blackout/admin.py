from django.contrib import admin
from .models import *

class WebsiteAdmin(admin.ModelAdmin):
	list_display = ('origin', 'created_at', 'updated_at')

admin.site.register(Website, WebsiteAdmin)
